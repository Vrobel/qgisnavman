### About plugin
This plugin prepare enviroment for running script prepare_navigation and line_splitter in QGIS. 
Plugin can processing several times with batch function. In the end of every iteration plugin merge final shape files into one layer and render it. 

Plugin uses Pandas, GeoPandas and Fiona modules that are not available in default installation of QGIS.
Please install QGIS over OSGEO4W installer with additional python modules, or install modules separately (not recommended).
Plugin was tested on QGIS 3.4(Madeira) and QGIS 3.8(Zanzibar)
https://download.osgeo.org/osgeo4w/osgeo4w-setup-x86_64.exe




This will create a directory structure `AB-6m_x_3/My_farm/[farm]/[field]/`, where `farm` and `field` will be taken from the corresponding attribute fields in `plots.shp`.

Files `__N__E0H.pos, Boundary.shp, Swaths.shp, LineFeature.shp, AreaFeature.shp` will be produced where applicable.

This script run will:
* convert input geometry to EPSG 5514 (S-JTSK) from their respective CRS (as specified with "crs" parameter in `config.json`) or the default **4326**
* load polygons from `plots.shp`, remove their slivers with 0.1 m tolerance and remove holes smaller than 300 m²
* create 3 swaths for 6 m swath width from these polygons
* swath distances will be equal to 3, 9 and 15 meters
* swaths are then simplified with 0.1 m tolerance
* and cut in vertices with angles smaller than 120 ° or where turning from one segment to another is impossible with turning radius of 10 m or larger
* remove swath segments shorter than 4 m
* create a folder structure as described above
* create files `Boundary.shp, Swaths.shp, LineFeature.shp, AreaFeature.shp` and a file `XX.XXXXNYY.YYYYE0H.pos` with WGS84 coordinates of a representative point for each field

#### line_splitter.py

Accepts a GeoDataFrame containing LineStrings as input and returns the GeoDataFrame with geometries split according to specified parameters. 