#!/usr/bin/env python
# coding: utf-8
import math
import numpy as np
from numpy.lib import NumpyVersion

from shapely.ops import split, linemerge
from shapely.geometry import LineString, Point

TURN_LIMIT_DISTANCE = 4.5
TURN_LIMIT_ANGLE = 90

def azimuth(point1, point2):
    angle = math.atan2(point2[0] - point1[0], point2[1] - point1[1])
    return (math.degrees(angle) + 360) % 360

def steep_corner(P, A, B, limit):
    ## limit je string
   # print (type(limit))
   # print (type(P))
   # print (type(A))
   # print (type(B))
    d_azimuth = abs(azimuth(P, A) - azimuth(P, B))
    if min(d_azimuth, abs(360 - d_azimuth)) <= limit:
        return True
    
    return False

def perpendicular_line(A, B, C, d):
    line = LineString([A, B])
    if NumpyVersion(np.__version__) < '1.50.0': 
        v = np.flip(B - A, 0) * np.array([1, -1]) / line.length
    else:
        v = np.flip(B - A) * np.array([1, -1]) / line.length
    #print (type(A))
    #print (type(B))
    #print (type(C))
    #print (type(d))
    return LineString([C + v * d/2, C - v * d/2])

def impassable_corner(P, A, B, limit):
    l1 = LineString([P, A])
    l2 = LineString([P, B])
    
    a, b = [l1, l2] if l1.length < l2.length else [l2, l1]
    
    a_C = a.centroid
    b_C = b.interpolate(a.length / 2)

    a_p = perpendicular_line(np.array(P[:2]), np.array(a.coords[1][:2]), np.array(a_C.coords[0][:2]), limit * 2)
    b_p = perpendicular_line(np.array(P[:2]), np.array(b.coords[1][:2]), np.array(b_C.coords[0][:2]), limit * 2)
    
    return a_p.intersects(b_p)

def find_impassable_corners(line, coords, turn_limit, angle_limit):
    impassable_corners = []
    
    for i in range(1, len(coords) - 1):
    # ignore the first point to avoid complicated recursion later
        P = coords[i]
        A = coords[i - 1]
        B = coords[i + 1]
        
        impassable_corners += [i] if impassable_corner(P, A, B, turn_limit) or steep_corner(P, A, B, angle_limit) else []

    return impassable_corners[::-1]
    # reverse the output list to be able to call .pop() on it recursively

def split_line_segments(line_segments, impassable_corners, coords):
    if not impassable_corners:
        return line_segments
    
    corner = Point(coords[impassable_corners.pop()])
    
    line_segments = line_segments[:-1] + list(split(line_segments[-1], corner))
            
    return split_line_segments(line_segments, impassable_corners, coords)

def create_passable_lines(lines, turn_limit=TURN_LIMIT_DISTANCE, angle_limit=TURN_LIMIT_ANGLE):
    """Ensure the provided lines are passable by vehicles with limited
    turning capabilities.

    Input lines are split in vertices which are evaluated as impassable.
    
    A vertex is considered impassable when:
    * its angle exceeds provided threshold
    * a turn from a longer segment to the centroid of a shorter one
        is not possible with the provided turn radius threshold

    If the line is split in one or more vertices and the line begins
    and ends with the same vertex and that vertex is passable, the first
    and the last split segment is joint into a single one.

    Parameters
    ----------
    lines : geopandas.geodataframe.GeoDataFrame
        GeoDataFrame containing LineString geometries in the "geometry"
        column
    turn_limit : float [meters], optional
        Radius of the tightest possible turn with the target vehicle
        (default is 4.5)
    angle_limit : float [degrees], optional
        The highest angle of a vertex, which is not accepted as passable
        (default is 90)

    Returns
    -------
    geopandas.geodataframe.GeoDataFrame
        Each row of the GeoDataFrame represents one split segment of the
        input lines. Segment attributes are copied from the original line,
        geometry is a single shapely LineString.
    """

    passable_lines = lines[0:0]
    # empty dataframe with the same columns as the original `lines`
    turn_limit = 4.5 ## test 
    angle_limit = 90
    for i, line in lines.iterrows():
        line_g = line.geometry
        coords = line_g.coords
        
        impassable_corners = find_impassable_corners(line_g, coords, turn_limit, angle_limit)
        
        line_split = split_line_segments([line_g], impassable_corners, coords)
        
        if len(line_split) > 1 and coords[0] == coords[-1]:
        # now we can check if the line should be joint at the start (if it's possible)
            P = coords[0]
            A = coords[-2]
            B = coords[1]

            if not steep_corner(P, A, B, angle_limit) and not impassable_corner(P, A, B, turn_limit):
                line_split = [linemerge(line_split[0].union(line_split[-1]))] + line_split[1: -1]
                
        for split_line in line_split:
            new_line = line
            new_line.geometry = split_line
            passable_lines = passable_lines.append(new_line)
            
    return passable_lines
