#!/usr/bin/env python
# coding: utf-8

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-c", "--config", type=str, default="default.json", help="Config JSON file with input files information.")
parser.add_argument("-o", "--output-folder", type=str, default="result", help="Folder name in which to create the resulting AgGPS/Data/ folder structure")
parser.add_argument("-f1", "--folder1", type=str, default="Default_Organization", help="Organization name that will be used as the name of the folder on the 3rd level.")
parser.add_argument("--folder2", type=str, default="Default_Farm", help="Farm name that will be used as the name of the folder on the 4th level.")
parser.add_argument("--folder3", type=str, default="Default_Field", help="Field name that will be used as the name of the folder on the 5th level.")
parser.add_argument("-r", "--radius", type=float, default=4.5, help="Radius limit for line splitting.")
parser.add_argument("-a", "--angle", type=float, default=90.0, help="Angle limit for line splitting.")
parser.add_argument("--simplify-limit", type=float, help="Limit used for simplification of input split lines.")
parser.add_argument("--sliver-limit", type=float, default=0, help="Limit used for removal of polygon slivers.")
parser.add_argument("--hole-buffer-limit", type=float, help="Limit used for creating swaths around polygon holes.")
parser.add_argument("-s", "--swath-width", type=float, default=36, help="Width of the swath of the machinery.")
parser.add_argument("-hl", "--headland-count", type=int, default=0, help="Number of parallel lines to generate around field boundaries.")
parser.add_argument("-l,", "--length-limit", type=float, default=0, help="Length limit for split lines.")
parser.add_argument("--replace", action="store_true", help="Erase the content of output-folder first.")
parser.add_argument("--crs", type=int, default=5514, help="Coordinate reference system used for geometric calculations.")
parser.add_argument("--crs-out", type=int, default=4326, help="Coordinate reference system of output data.")
args = parser.parse_args()

import shapely
from shapely.geometry import Polygon, MultiPolygon

import numpy as np
import pandas as pd
import geopandas as gpd
from fiona.crs import from_epsg
import shutil
from os import path, makedirs
import json
import sys

pd.options.mode.chained_assignment = None

import datetime

NOW = datetime.datetime.today()
DATE_NOW = NOW.strftime('%Y-%m-%d')
TIME_NOW = NOW.strftime('%I:%M:%S%p')

S_FROM = ' ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝßàáâãäåçèéêëìíîïñòóôõöùúûüýÿĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĶķĸĹĺĻļĽľĿŀŁłŃńŅņŇňŉŊŋŌōŎŏŐőŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻżŽžſ'
S_TO = '_AAAAAACEEEEIIIINOOOOOUUUUYsaaaaaaceeeeiiiinooooouuuuyyAaAaAaCcCcCcCcDdDdEeEeEeEeEeGgGgGgGgHhHhIiIiIiIiIiKkkLlLlLlLlLlNnNnNnNnNOoOoOoRrRrRrSsSsSsSsTtTtTtUuUuUuUuUuUuWwYyYZzZzZzs'
S_PAIRS = {S_FROM[i]: S_TO[i] for i in range(len(S_TO))}

def replace_characters(string, S_FROM=S_FROM, S_TO=S_TO, S_PAIRS=S_PAIRS):
    for CHAR in S_FROM:
        string = string.replace(CHAR, S_PAIRS[CHAR])
        
    return string

if path.exists(args.output_folder):
    if args.replace:
        shutil.rmtree(args.output_folder)
    elif path.exists(f"{args.output_folder}/AgGPS/Data/{args.folder1}"):
        sys.exit(f"Output folder {args.output_folder}/AgGPS/Data/{args.folder1} already exists!")

from line_splitter import create_passable_lines

def load_input(group, directory="data"):
    result = gpd.GeoDataFrame()

    for item in group:
        file_path = path.join(directory, item["file"])

        if path.exists(file_path):
            crs = item["crs"] if "crs" in item else 4326

            new_input = gpd.read_file(file_path, geometry='geometry', crs=from_epsg(crs))

            if crs != args.crs:
                new_input = new_input.to_crs(args.crs)
                print(f"{item['file']} transformed to EPSG {args.crs}.")
            else:
                print(f"{item['file']} loaded.")

            for key, value in item["attributes"].items():
                new_input[key] = value

            result = result.append(new_input, sort=False)

        else:
            print(f"{file_path} does not exist!")
    return result

def parse_passable_lines(lines, radius, angle):
    if isinstance(lines, gpd.GeoDataFrame) and not lines.empty:
        lines_c = lines.copy()
        
        if args.simplify_limit:
            lines_c["geometry"] = lines_c["geometry"].simplify(args.simplify_limit)
            print(f"Lines simplififed with {args.simplify_limit} m limit.")
    
        passable_lines = create_passable_lines(lines_c, radius, angle)

        if args.length_limit > 0:
            passable_lines = passable_lines[passable_lines.geometry.length > args.length_limit]

        return passable_lines

print(f"Loading {args.config} configuration.")

with open(args.config) as config_file:
    config = json.loads(config_file.read())

cfg_path = path.split(args.config)[0]

boundaries = load_input([config["Boundary"]], cfg_path) if "Boundary" in config else None

boundaries_crs = boundaries.crs

if args.sliver_limit > 0:
    boundaries["geometry"] = boundaries.geometry.buffer(-args.sliver_limit, 1, join_style=2).buffer(args.sliver_limit, 1, join_style=2)
    print(f"Removed slivers with {args.sliver_limit} m limit.")

def remove_polygon_holes(row, hole_limit=None):
    if isinstance(row["geometry"], shapely.geometry.multipolygon.MultiPolygon):
        row["geometry"] = MultiPolygon([Polygon(p.exterior, list(filter(lambda x: Polygon(x).area > hole_limit, p.interiors)) if hole_limit else []) for p in row["geometry"]])
    else:
        row["geometry"] = Polygon(row["geometry"].exterior, list(filter(lambda x: Polygon(x).area > hole_limit, row["geometry"].interiors)) if hole_limit else [])

    return row

if args.hole_buffer_limit:
    boundaries = boundaries.apply(remove_polygon_holes, axis=1, hole_limit=args.hole_buffer_limit)
    print(f"Removed polygon holes smaller than {args.hole_buffer_limit} m².")

boundary_swaths = gpd.GeoDataFrame()

if args.headland_count > 0 and args.swath_width > 0:
    for n in range(args.headland_count):
        swath_distance = (n + 0.5) * args.swath_width

        boundary_buffers = boundaries.copy()
        boundary_buffers["geometry"] = boundary_buffers.geometry.buffer(-swath_distance)

        for i, row in boundary_buffers.iterrows():
            if not row.geometry.is_empty:
                row_geometries = row["geometry"].boundary
                row_geometries = list(row_geometries) if isinstance(row_geometries, shapely.geometry.multilinestring.MultiLineString) else [row_geometries]

                for geom in row_geometries:
                    row["geometry"] = geom
                    boundary_swaths = boundary_swaths.append(row)

        boundary_swaths["Id"] = 9

boundary_swaths.crs = boundaries_crs

boundaries = boundaries.apply(remove_polygon_holes, axis=1)
boundaries.crs = boundaries_crs

if boundaries.empty:
    sys.exit(f"{config['Boundary']['file']} is empty!")

if "folder2_field" in config["Boundary"] and config["Boundary"]["folder2_field"] not in boundaries.columns:
    sys.exit(f"Column {config['Boundary']['folder2_field']} is not present in {config['Boundary']['file']}!")

if "folder3_field" in config["Boundary"] and config["Boundary"]["folder3_field"] not in boundaries.columns:
    sys.exit(f"Column {config['Boundary']['folder3_field']} is not present in {config['Boundary']['file']}!")

swaths = boundary_swaths.append(load_input(config["Swaths"], cfg_path) if "Swaths" in config else None, sort=False)
line_features = load_input(config["LineFeature"], cfg_path).reset_index(drop=True).explode().reset_index(drop=True) if "LineFeature" in config else None

area_features = load_input(config["AreaFeature"], cfg_path) if "AreaFeature" in config else None

swaths_l_before = len(swaths)
swaths = swaths.reset_index(drop=True).explode().reset_index(drop=True) if isinstance(swaths, gpd.GeoDataFrame) else swaths

if isinstance(swaths, gpd.GeoDataFrame):
    print(f"Processed {len(swaths) - swaths_l_before} multipart lines to singlepart.")

line_features_s = parse_passable_lines(line_features, args.radius, args.angle)

if isinstance(line_features, gpd.GeoDataFrame) and isinstance(line_features_s, gpd.GeoDataFrame):
    print(f"Processed {len(line_features)} LineFeatures to {len(line_features_s)} passable lines.")


swaths_s = parse_passable_lines(swaths, args.radius, args.angle)

if isinstance(swaths, gpd.GeoDataFrame) and isinstance(swaths_s, gpd.GeoDataFrame):
    print(f"Processed {len(swaths)} Swaths to {len(swaths_s)} passable lines.")


def export_output(output=args.output_folder, boundaries=boundaries, swaths=swaths_s, line_features=line_features_s, area_features=area_features):
    if not config["Boundary"]["folder2_field"] and not config["Boundary"]["folder3_field"]:
        makedirs(f"{replace_characters(args.output_folder)}/AgGPS/Data/{replace_characters(args.folder1)}/{replace_characters(args.folder2)}/{replace_characters(args.folder3)}")

        return

    boundary_cols = ["Date", "Time", "Version", "Id", "Name", "Area", "Perimeter", "SwathsIn", "Dist1", "Dist2", "PrefWeight", "Farm", "geometry"]

    boundaries_r = boundaries.rename(columns={config["Boundary"]["folder2_field"]: "Farm", config["Boundary"]["folder3_field"]: "Name"})

    boundaries_t = boundaries_r[list(set(boundary_cols).intersection(boundaries_r.columns))]
    boundaries_t["Date"] = boundaries_t["Date"].fillna(DATE_NOW) if "Date" in boundaries_t.columns else DATE_NOW
    boundaries_t["Time"] = boundaries_t["Time"].fillna(TIME_NOW) if "Time" in boundaries_t.columns else TIME_NOW
    boundaries_t["Version"] = boundaries_t["Version"].fillna(42.0) if "Version" in boundaries_t.columns else 42.0
    boundaries_t["Area"] = boundaries_t.geometry.area
    boundaries_t["Perimeter"] = boundaries_t.geometry.length
    # perimeter also includes the perimeter of all holes!
    boundaries_t["SwathsIn"] = boundaries_t["SwathsIn"].fillna(0) if "SwathsIn" in boundaries_t.columns else 0
    boundaries_t["Dist1"] = boundaries_t["Dist1"].fillna(0) if "Dist1" in boundaries_t.columns else 0
    boundaries_t["Dist2"] = boundaries_t["Dist2"].fillna(0) if "Dist2" in boundaries_t.columns else 0
    boundaries_t["PrefWeight"] = boundaries_t["PrefWeight"].fillna(0) if "PrefWeight" in boundaries_t.columns else 0

    boundaries_t = boundaries_t[boundary_cols]

    swaths_cols = ["Date", "Time", "Version", "Id", "Name", "Length", "Dist1", "Dist2", "PrefWeight", "geometry"]

    if isinstance(swaths, gpd.GeoDataFrame) and not swaths.empty:
        swaths_t = swaths[list(set(swaths_cols).intersection(swaths.columns))]
        swaths_t["Date"] = swaths_t["Date"].fillna(DATE_NOW) if "Date" in swaths_t.columns else DATE_NOW
        swaths_t["Time"] = swaths_t["Time"].fillna(TIME_NOW) if "Time" in swaths_t.columns else TIME_NOW
        swaths_t["Version"] = swaths_t["Version"].fillna(42.0) if "Version" in swaths_t.columns else 42.0
        swaths_t["Name"] = swaths_t["Name"] if "Name" in swaths_t.columns else np.nan
        swaths_t["Length"] = swaths_t.geometry.length
        swaths_t["Dist1"] = swaths_t["Dist1"].fillna(0) if "Dist1" in swaths_t.columns else 0
        swaths_t["Dist2"] = swaths_t["Dist2"].fillna(0) if "Dist2" in swaths_t.columns else 0
        swaths_t["PrefWeight"] = swaths_t["PrefWeight"].fillna(0) if "PrefWeight" in swaths_t.columns else 0

        swaths_t = swaths_t[swaths_cols]

    line_features_cols = ["Date", "Time", "Version", "Id", "Name", "Length", "Dist1", "Dist2", "PrefWeight", "geometry"]

    if isinstance(line_features, gpd.GeoDataFrame) and not line_features.empty:
        line_features_t = line_features[list(set(line_features_cols).intersection(line_features.columns))]
        line_features_t["Date"] = line_features_t["Date"].fillna(DATE_NOW) if "Date" in line_features_t.columns else DATE_NOW
        line_features_t["Time"] = line_features_t["Time"].fillna(TIME_NOW) if "Time" in line_features_t.columns else TIME_NOW
        line_features_t["Version"] = line_features_t["Version"].fillna(42.0) if "Version" in line_features_t.columns else 42.0
        line_features_t["Name"] = line_features_t["Name"] if "Name" in line_features_t.columns else np.nan
        line_features_t["Length"] = line_features_t.geometry.length
        line_features_t["Dist1"] = line_features_t["Dist1"].fillna(0) if "Dist1" in line_features_t.columns else 0
        line_features_t["Dist2"] = line_features_t["Dist2"].fillna(0) if "Dist2" in line_features_t.columns else 0
        line_features_t["PrefWeight"] = line_features_t["PrefWeight"].fillna(0) if "PrefWeight" in line_features_t.columns else 0

        line_features_t = line_features_t[line_features_cols]

    area_features_cols = ["Date", "Time", "Name", "Id", "Area", "Perimeter", "geometry"]

    if isinstance(area_features, gpd.GeoDataFrame) and not area_features.empty:
        area_features_t = area_features[list(set(area_features_cols).intersection(area_features.columns))]
        area_features_t["Date"] = area_features_t["Date"].fillna(DATE_NOW) if "Date" in area_features_t.columns else DATE_NOW
        area_features_t["Time"] = area_features_t["Time"].fillna(TIME_NOW) if "Time" in area_features_t.columns else TIME_NOW
        area_features_t["Name"] = area_features_t["Name"] if "Name" in area_features_t.columns else np.nan
        area_features_t["Area"] = area_features_t.geometry.area
        area_features_t["Perimeter"] = area_features_t.geometry.length

        area_features_t = area_features_t[area_features_cols]

    for i in range(len(boundaries_t)):
        boundary = boundaries_t[boundaries_t.index == i]
        row = boundaries.loc[i]

        field_dir = f"{replace_characters(args.output_folder)}/AgGPS/Data/{replace_characters(args.folder1)}/{replace_characters(boundary.Farm.item())}/{replace_characters(boundary.Name.item())}"

        field_dir += f'_{i}' if sum(boundaries_t.Name == boundary.Name.item()) > 1 else ''

        if sum(boundaries_t.Name == boundary.Name.item()) > 1:
            print(field_dir)

        makedirs(field_dir)

        line_o = boundary.drop(columns='Farm')

        if isinstance(swaths, gpd.GeoDataFrame) and not swaths_t.empty:
            swaths_o = swaths_t[swaths_t.intersects(line_o.geometry.item())]
            # line_o.SwathsIn = len(line_out.geometry.interiors) # will not work because of MultiPolygons
            line_o.SwathsIn = len(swaths_o) # might be correct? → TODO: MiKraus check
            swaths_o.Name = swaths_o.Name.fillna(line_o.Name.item())
            if args.crs != args.crs_out:
                swaths_o = swaths_o.to_crs(epsg=args.crs_out)

            if not swaths_o.empty:
                swaths_o.to_file(path.join(field_dir, "Swaths.shp"))
        
        if isinstance(line_features, gpd.GeoDataFrame) and not line_features_t.empty:
            line_features_o = line_features_t[line_features_t.intersects(line_o.geometry.item())]
            line_features_o.Name = line_features_o.Name.fillna(line_o.Name.item())
            if args.crs != args.crs_out:
                line_features_o = line_features_o.to_crs(epsg=args.crs_out)

            if not line_features_o.empty:
                line_features_o.to_file(path.join(field_dir, "LineFeature.shp"))
        
        if isinstance(area_features, gpd.GeoDataFrame) and not area_features_t.empty:
            area_features_o = area_features_t[area_features_t.intersects(line_o.geometry.item())]
            area_features_o.Name = area_features_o.Name.fillna(line_o.Name.item())
            if args.crs != args.crs_out:
                area_features_o = area_features_o.to_crs(epsg=args.crs_out)

            if not area_features_o.empty:
                area_features_o.to_file(path.join(field_dir, "AreaFeature.shp"))

        if args.crs != args.crs_out:
            line_o = line_o.to_crs(epsg=args.crs_out)
        r_point = line_o.representative_point().item().coords[0]

        print(f"Writing {field_dir}/{r_point[0]:.4f}E{r_point[1]:.4f}N0H.pos")
        open(path.join(field_dir, f"{r_point[0]:.4f}E{r_point[1]:.4f}N0H.pos"), 'a').close()
        # write auxiliary file with the coordinates of a representative point of the field geometry fixed to 4 decimal places (and 0H = altitude 0)

        if not line_o.empty:
            line_o.to_file(path.join(field_dir, "Boundary.shp"))

export_output()