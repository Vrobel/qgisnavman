class Batch(object):
    def __init__(self):
        self.swaths_width = None
        self.headland_count = None
        self.boundary = None
        self.boundaryID = None
        self.swaths = list()
        self.swathsID = None
        self.areaID = None
        self.area = list()
        self.lineID =None
        self.line =list()
        self.farmName = None
        self.output = None

        self.lengthLimit = None
        self.folder2 = None
        self.folder3 = None
        self.simplifyLimit = None
        self.holeBuffer = None
        self.sliverLimit = None
        self.radius = None
        self.angle = None
        self.crs = None

######## get
    def getFolder2(self):
        return self.folder2
    def getBoundaryID(self):
        return self.boundaryID
    def getBoundaryPath(self):
        return self.boundary
    def getFolder3(self):
        return self.folder3
    def getSimplifyLimit(self):
        return self.simplifyLimit
    def getHoleBuffer(self):
        return self.holeBuffer
    def getSliverLimit(self):
        return self.sliverLimit
    def getRadius(self):
        return self.radius
    def getAngle(self):
        return self.angle
    def getLengthLimit(self):
        return self.lengthLimit
    def getCrs(self):
        return self.crs
    def getHeadlandCount(self):
        return self.headland_count

    def getSwathWidth(self):
        return self.swaths_width
    def getFarmName(self):
        return self.farmName

    def getOutputFolder(self):
        return self.output
    def getSwaths(self):
        return self.swaths

    def getLine(self):
        return self.line
    def getArea(self):
        return self.area
    def getAreaID(self):
        return self.areaID
    def getLineID(self):
        return self.lineID
    def getSwathsID(self):
        return self.swathsID

######### set
    def setHeadlandCount(self, headland_count):
        self.headland_count = headland_count
    def setSwathsWidth(self, swaths_width):
        self.swaths_width = swaths_width

    def setFarmName(self, farmName):
        self.farmName = farmName

  
    def setOutput(self, output):
        self.output = output
    def setLine(self, line, ID):
        self.line.append(line)
        self.lineID = ID
    def setArea(self, area, ID):
        self.area.append(area)
        self.areaID = ID
    def setSwaths(self, swaths, ID):
        self.swaths.append(swaths) 
        self.swathsID = ID
    def setBoundary(self, boundary, ID):
        self.boundary = (boundary)
        self.boundaryID = ID
    def setFolder2(self, folder2):
        self.folder2 = folder2
    def setFolder3(self, folder3):
        self.folder3 = folder3
    def setSimplifyLimit(self, simplifyLimit):
        self.simplifyLimit = simplifyLimit
    def setHoleBuffer(self, holeBuffer):
        self.holeBuffer = holeBuffer
    def setSliverLimit(self, sliverLimit):
        self.sliverLimit = sliverLimit
    def setRadius(self, radius):
        self.radius = radius
    def setAngle(self, angle):
        self.angle = angle
    def setCrs(self, crs):
        self.crs = crs
    def setLengthLimit(self, lengthLimit):
        self.lengthLimit = lengthLimit